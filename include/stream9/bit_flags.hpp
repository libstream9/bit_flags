#ifndef STREAM9_XDG_MIME_BIT_FLAGS_HPP
#define STREAM9_XDG_MIME_BIT_FLAGS_HPP

#include <type_traits>
#include <utility>

namespace stream9 {

template<typename E>
    requires (std::is_enum_v<E>)
          && (std::is_unsigned_v<std::underlying_type_t<E>>)
          && (!std::is_const_v<E>)
          && (!std::is_volatile_v<E>)
class bit_flags
{
    using value_t = std::underlying_type_t<E>;

public:
    constexpr bit_flags() noexcept = default;
    constexpr bit_flags(E const v) noexcept
        : m_bits { static_cast<value_t>(v) } {}

    constexpr bit_flags& operator|=(bit_flags const& other) noexcept
    {
        m_bits |= static_cast<value_t>(other.m_bits);
        return *this;
    }

    constexpr bit_flags operator|(bit_flags const& other) const noexcept
    {
        return bit_flags(*this) |= other;
    }

    constexpr bit_flags& operator&=(bit_flags const& other) noexcept
    {
        m_bits &= static_cast<value_t>(other.m_bits);
        return *this;
    }

    constexpr bit_flags operator&(bit_flags const& other) const noexcept
    {
        return bit_flags(*this) &= other;
    }

    constexpr bit_flags& operator^=(bit_flags const& other) noexcept
    {
        m_bits ^= static_cast<value_t>(other.m_bits);
        return *this;
    }

    constexpr bit_flags operator^(bit_flags const& other) const noexcept
    {
        return bit_flags(*this) ^= other;
    }

    constexpr bool operator==(E const& other) const noexcept
    {
        return m_bits == static_cast<value_t>(other);
    }

    constexpr operator bool () const noexcept
    {
        return m_bits != static_cast<value_t>(E());
    }

private:
    value_t m_bits = static_cast<value_t>(E());
};

} // namespace stream9

#endif // STREAM9_XDG_MIME_BIT_FLAGS_HPP
